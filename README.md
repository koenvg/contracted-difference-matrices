Contracted Difference Matrices
==============================

This repository contains a Python 3 library for working with contracted difference matrices, as introduced by Koen van Greevenbroek and Jonathan Jedwab.