#!/usr/bin/env python3

import multiprocessing as mp
import sys
import copy
import time
from itertools import product, combinations, starmap, permutations, \
    chain
from collections import Counter
from functools import reduce
from operator import mul
from math import log, gcd
import random


# ==========================
# === Printing functions ===
# ==========================

def format_row(row, sep):
    """
    Output a string containing the given row, formatted nicely.
    Elements of the row are separated by a single space, and
    components of each element are separated by the given string
    'sep'."""
    s = ""
    for tup in row:
        s += sep.join(map(str, tup))
        s += " "
    return s.strip()

                
def print_DM(M, sep=""):
    """
    Prints the given difference matrix to the standard output, nicely
    formatted. Components of elements optionally separated by 'sep'.
    """
    for row in M:
        print(format_row(row, sep=sep))

        
# =========================
# === Reading functions ===
# =========================

def read_DM(s, group=None, mult=1, sep=""):
    """
    Reads a difference matrix from a string, and returns the
    difference matrix in the internal representation (a nested list of
    tuples). If the group is specified but the given matrix is not a
    difference matrix over than group, an error is thrown.

    This functions expects a string where rows are separated by
    newlines, entries by whitespace, and individual coordinates by
    `sep'.
    """
    # Can't have an empty difference matrix:
    s = s.strip()
    if not s: raise ValueError("read_DM: got empty string.")

    # Calculate some parameters so we can check their invariance later.
    first_row = s.split("\n")[0]
    width = len(first_row.split())
    first_entry = first_row.split()[0]
    rank = len(first_entry.split(sep)) if sep else len(first_entry)

    M = []
    for line in s.split("\n"):
        if len(line.split()) != width:
            raise ValueError("read_DM: some rows don't have the same"
                             "length.")
        row = []
        for word in line.split():
            coords = word.split(sep) if sep else word
            if len(coords) != rank:
                raise ValueError("read_DM: some entries don't have "
                                 "the same rank")
            row.append(tuple(int(x) for x in coords))
        M.append(row)

    # Is M even a difference matrix?
    if group and not is_DM(M, group, mult):
        raise ValueError("read_DM: the given matrix is not a "
                         "difference matrix.")
        
    return M


def read_DM_file(f, sep="", group=None, mult=1):
    """
    Read a number of difference matrices from a file, and returns a
    list of the difference matrices. Each difference matrix must be
    separated by two or more consecutive newlines.
    """
    DMs = []
    file_s = open(f, 'r').read()
    DMs_s = [DM for DM in file_s.split("\n\n") if DM]
    for DM in DMs_s:
        DMs.append(read_DM(DM, sep=sep, group=group, mult=mult))
    return DMs

        
# =========================
# === Utility functions ===
# =========================

def get_p(g):
    """
    If the given group g is a p-group, return p. Otherwise throw an
    error.
    """
    # Is the underlying group a p-group?
    if reduce(gcd, g) == 1:
        raise ValueError("get_p: the given group is not a p-group.")
    # The group is a p-group. Find p.
    p = 2
    while (g[0] % p != 0): p += 1
    return p

def g_size(g):
    """
    Returns the size of the group g.
    """
    return reduce(mul, g, 1)

def id_elem(g):
    """
    Return the identity element of g.
    """
    return tuple([0] * len(g))

def id_row(g, contr=False):
    """
    Return a row with |g| identity elements.
    """
    s = g_size(g) if not contr else round(log(g_size(g), get_p(g)))
    return [id_elem(g)] * s

def add_elems(a, b, g):
    """
    Return the sum of two elements in g.
    """
    return tuple((a[i] + b[i]) % g[i] for i in range(len(a)))

def add_elem_list(elems, g):
    """
    Return the sum of the given list of elements in g.
    """
    return tuple(reduce(lambda a, b: add_elems(a, b, g), elems, id_elem(g)))

def negate_elem(a, g):
    """
    Return the inverse a in g.
    """
    return tuple(-a[i] % g[i] for i in range(len(a)))

def diff_elems(a, b, g):
    """
    Return the difference between two elements in g.
    """
    return add_elems(a, negate_elem(b, g), g)

def scale_elem(a, k, g):
    """
    Return the k * a where a is in g, and k is an integer scalar.
    """
    return tuple((e * k) % d for (e,d) in zip(a, g))

def add_rows(r1, r2, g):
    """
    Return the elementwise sum of rows r1 and r2 over g.
    """
    return [add_elems(r1[k], r2[k], g) for k in range(len(r1))]

def diff_rows(r1, r2, g):
    """
    Return the elementwise difference of rows r1 and r2 over g.
    """
    return add_rows(r1, scale_row(r2, -1, g), g)

def add_row_list(rs, g, contr=False):
    """
    Return the elementwise sum of all the rows in the list rs, over g.
    """
    return reduce(lambda r1, r2: add_rows(r1, r2, g), rs,
                  id_row(g, contr=contr))

def scale_row(r, k, g):
    """
    Return the row r, elementwise scaled by integer scalar k over g.
    """
    return [scale_elem(e, k, g) for e in r]


# ===========================
# === Difference matrices ===
# ===========================

def is_DM(M, group, mult=1, debug=False):
    """
    Check if the matrix M is a difference matrix over 'group'.
    """
    for i in range(len(M)):
        for j in range(i + 1, len(M)):
            diff = diff_rows(M[i], M[j], group)
            c = Counter(diff)
            mults = list(c.values())
            if mults[0] != mult or len(mults) != mults.count(mults[0]):
                if debug:
                    print("is_DM: failed at difference between rows "
                          "{} and {}. Difference:".format(i, j))
                    print(format_row(diff, sep=""))
                return False
    return True


# ======================================
# === Contracted difference matrices ===
# ======================================

def to_base(n, b):
    """
    Returns the list of digits of n in base b
    """
    digits = []
    while n > 0:
        digits.append(n % b)
        n = n // b
    digits.reverse()
    return digits


def expand_row(row, group):
    """
    Return the p-expansion of 'row'.

    Raises an error if the underlying group is not a p-group.
    """
    p = get_p(group)    
    exp_row = []
    for i in range(p**len(row)):
        # Use base p representation of i to determine which elements
        # to add up
        indices = to_base(i, p)
        # Pad with enough 0s
        indices = [0] * (len(row) - len(indices)) + indices
        elem = add_elem_list([scale_elem(e, k, group)
                              for (e, k) in zip(row, indices)], group)
        exp_row.append(elem)
    return exp_row


def is_generating_set(elems, group):
    """
    Check if the given elements for a generating set for 'group', in
    the sense that their p-expansion contains every element of 'group'
    exactly once.
    """
    generated = expand_row(elems, group)
    if len(generated) == len(set(generated)):
        return True
    else:
        return False


def generating_sets(group):
    """
    Returns a generator for all the generating sets for 'group'.
    """
    p = get_p(group)
    all_elems = list(product(*[range(n) for n in group]))[1:]
    num_generators = sum(map(lambda x: int(log(x, p)), group))
    for comb in combinations(all_elems, num_generators):
        if is_generating_set(list(comb), group):
            for c in permutations(comb):
                yield list(c)

                
def generating_sets_random(group):
    """
    Returns a generator that draws random generating sets of 'group'
    """
    p = get_p(group)
    all_elems = list(product(*[range(n) for n in group]))[1:]
    num_generators = sum(map(lambda x: int(log(x, p)), group))
    C = list(combinations(all_elems, num_generators))
    while True:
        random.shuffle(C)
        for comb in C:
        # comb = random.choice(C)
            if is_generating_set(list(comb), group):
                yield list(random.choice(list(permutations(comb))))


def transpose(M):
    """
    Return the transpose of the matrix M.
    """
    return list(zip(*M))


def expand_DM(M, group):
    """
    Return the p-expansion of M over 'group'.
    """
    M1 = transpose([expand_row(row, group) for row in transpose(M)])
    return [expand_row(row, group) for row in M1]


def is_CDM(M, group, s=0, debug=False):
    """
    Checks if a given matrix is a contracted difference matrix.
    """
    p = get_p(group)
    return is_DM(expand_DM(M, group), group, mult=p**s, debug=debug)


def get_combs(M, group, contr=True):
    """
    Returns a list with all possible combinations of rows of M. That
    is, the list contains every possible sum of the form
    a_1 R_1 + ... + a_n R_n, where the R_i are the rows of M and the
    a_i are integer coefficients strictly between -p and p.
    """
    p = get_p(group)
    comb_indices = list(product(*([range(-p+1, p)] * len(M))))
    combs = [add_row_list([scale_row(row, k, group)
                           for (row, k) in zip(M, idx)],
                          group, contr=contr)
             for idx in comb_indices]
    return combs


def is_CDM_partial(M, combs, group):
    """
    Checks if a given matrix is a contracted difference matrix,
    assuming all rows but the last row already work. Returns the new
    combinations with the last row if successful, or None otherwise.

    M: the matrix to check.
    combs: all combinations of all but the last row.
    """
    p = get_p(group)
    new_combs = [M[-1]]
    for row in combs:
        for i in range(-p+1, p):
            if i == 0: continue
            c = add_rows(row, scale_row(M[-1], i, group), group)
            if is_generating_set(c, group):
                new_combs.append(c)
            else:
                return None
    return new_combs


def max_CDM_rec(args):
    """
    Recursive helper function for the max_CDM function.
    Returns the contracted difference matrix with the most rows,
    given the starting matrix.
    """
    (M, combs, gen_sets, group) = args
    # Reached upper bound:
    if len(M) == round(log(g_size(group), get_p(group))):
        return M
    # Recursive case:
    max_M = M
    for row in gen_sets:
        new_M = M + [row]
        new_combs = is_CDM_partial(new_M, combs, group)
        if new_combs:
            max_M = max(max_M,
                        max_CDM_rec((new_M,
                                               combs + new_combs,
                                               gen_sets, group)),
                        key=len)
    return max_M


def max_CDM(group, init=None, progress=False,
                      output_file=None, num_cores=mp.cpu_count() - 1):
    """
    Computes the contracted difference matrix with the most rows over
    'group', using an exhaustive search.

    group: the group to perform the search over.
    init: an optional starting contracted difference matrix.
    progress: whether to display the progress, including the expected
        time until completion.
    output_file: optionally write all maximum difference matrix to
        this file.
    num_cores: the number of CPU cores to use in the search. Defaults
        to all cores but one.
    """
    # Prepare generating sets
    gen_sets = list(generating_sets(group))

    # Prepare for parallel processing
    pool = mp.Pool(num_cores)
    
    # Prepare arguments
    p = get_p(group)
    if init is None:
        # init = [gen_sets[0]]
        init = []
    else:
        if not is_CDM(init, group):
            raise ValueError("max_CDM: initial matrix is "
                             "not a contracted difference matrix.")
    init_combs = get_combs(init, group)
        
    rows = []
    combss = []
    for row in gen_sets:
        new_combs = is_CDM_partial(init + [row], init_combs, group)
        if new_combs:
            rows.append(row)
            combss.append(init_combs + new_combs)
    
    # Run main part of the search
    args = [(init + [rows[i]], combss[i], rows, group)
            for i in range(len(rows))]
    result = pool.imap(max_CDM_rec, args)

    # Gather results and write updates
    Ds = []
    num_rows = len(rows)
    start_time = time.time()
    for i, D in enumerate(result, 1):
        Ds.append(D)
        if progress:
            rows_per_sec = i / (time.time() - start_time)
            rem = int((num_rows - i) / (rows_per_sec * 60))
            sys.stdout.write("Checked {} out of {} rows, {} minutes "
                             "remaining\r".format(i, num_rows, rem))
    if progress: sys.stdout.write("\033[K")
    max_D = max(Ds + [init], key=len)

    # If output_file is specified, write all DMs to it
    if output_file:
        f = open(output_file, "w")
        for D in Ds:
            if len(D) == len(max_D):
                for r in D:
                    f.write(format_row(r, "", 1) + "\n")
                f.write("\n")
        f.close()

    pool.close()
    pool.join()

    return max_D


def greedy_init(d):
    """
    Helper function for the contracted difference matrix greedy search.
    """
    global done
    done = d

    
def max_CDM_greedy_single(args):
    """
    A single core helper function for max_CDM_greedy.
    """
    # Prepare parameters
    (group, row_limit, init, reset_limit) = args
    p = get_p(group)
    M = []
    rows = generating_sets_random(group)
    current_streak = reset_limit
    # Run main loop
    while len(M) < row_limit and not done.is_set():
        if current_streak >= reset_limit:
            if init:
                M = init
            else:
                M = [next(rows)]
            combs = get_combs(M, group)
            current_streak = 0
        row = next(rows)
        new_combs = is_CDM_partial(M + [row], combs, group)
        if new_combs:
            M.append(row)
            combs += new_combs
        current_streak += 1
    done.set()
    return M


def max_CDM_greedy(group, row_limit, init=None,
                             progress=False, reset_limit=10000,
                             timeout=None,
                             num_cores=mp.cpu_count() - 1):
    """
    Do a randomized greedy search for a contracted difference matrix
    over 'group'.
    
    row_limit: terminate the algorithm when reaching this many rows.
    reset_limit: how many rows should be tried on top of the (given)
        starting row. We reset so as to not get stuck on a row that's
        'bad'.
    timeout: upper bound on how long the algorithm should run.
    num_cores: how may CPU cores to use. Defaults to all cores but one.
    """
    done = mp.Event()
    pool = mp.Pool(num_cores, initializer=greedy_init(done))
    args = [(group, row_limit, init, reset_limit)
            for i in range(num_cores)]
    results = pool.imap(max_CDM_greedy_single, args)
    pool.close()
    pool.join()
    M = max(results, key=len)
    return M
